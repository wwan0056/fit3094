// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT3094_A1_CODE_Ship_generated_h
#error "Ship.generated.h already included, missing '#pragma once' in Ship.h"
#endif
#define FIT3094_A1_CODE_Ship_generated_h

#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_SPARSE_DATA
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_RPC_WRAPPERS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShip(); \
	friend struct Z_Construct_UClass_AShip_Statics; \
public: \
	DECLARE_CLASS(AShip, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AShip)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAShip(); \
	friend struct Z_Construct_UClass_AShip_Statics; \
public: \
	DECLARE_CLASS(AShip, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AShip)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShip(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShip) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShip); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShip); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShip(AShip&&); \
	NO_API AShip(const AShip&); \
public:


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShip(AShip&&); \
	NO_API AShip(const AShip&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShip); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShip); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AShip)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MoveSpeed() { return STRUCT_OFFSET(AShip, MoveSpeed); } \
	FORCEINLINE static uint32 __PPO__Tolerance() { return STRUCT_OFFSET(AShip, Tolerance); }


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_15_PROLOG
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_RPC_WRAPPERS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_INCLASS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_INCLASS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT3094_A1_CODE_API UClass* StaticClass<class AShip>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Ship_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
