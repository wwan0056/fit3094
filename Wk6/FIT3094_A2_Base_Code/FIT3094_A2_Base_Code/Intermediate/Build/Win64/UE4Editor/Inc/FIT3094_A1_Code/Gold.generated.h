// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT3094_A1_CODE_Gold_generated_h
#error "Gold.generated.h already included, missing '#pragma once' in Gold.h"
#endif
#define FIT3094_A1_CODE_Gold_generated_h

#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_SPARSE_DATA
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_RPC_WRAPPERS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGold(); \
	friend struct Z_Construct_UClass_AGold_Statics; \
public: \
	DECLARE_CLASS(AGold, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AGold)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGold(); \
	friend struct Z_Construct_UClass_AGold_Statics; \
public: \
	DECLARE_CLASS(AGold, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AGold)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGold(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGold) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGold); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGold); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGold(AGold&&); \
	NO_API AGold(const AGold&); \
public:


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGold(AGold&&); \
	NO_API AGold(const AGold&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGold); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGold); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGold)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_PRIVATE_PROPERTY_OFFSET
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_9_PROLOG
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_RPC_WRAPPERS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_INCLASS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_INCLASS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT3094_A1_CODE_API UClass* StaticClass<class AGold>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Gold_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
