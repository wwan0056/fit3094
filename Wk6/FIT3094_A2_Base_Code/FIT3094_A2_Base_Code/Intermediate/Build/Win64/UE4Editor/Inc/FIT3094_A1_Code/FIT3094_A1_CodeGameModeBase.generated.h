// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT3094_A1_CODE_FIT3094_A1_CodeGameModeBase_generated_h
#error "FIT3094_A1_CodeGameModeBase.generated.h already included, missing '#pragma once' in FIT3094_A1_CodeGameModeBase.h"
#endif
#define FIT3094_A1_CODE_FIT3094_A1_CodeGameModeBase_generated_h

#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_SPARSE_DATA
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetMapArray); \
	DECLARE_FUNCTION(execGetRandomMapText); \
	DECLARE_FUNCTION(execGetMapFileList);


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetMapArray); \
	DECLARE_FUNCTION(execGetRandomMapText); \
	DECLARE_FUNCTION(execGetMapFileList);


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFIT3094_A1_CodeGameModeBase(); \
	friend struct Z_Construct_UClass_AFIT3094_A1_CodeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AFIT3094_A1_CodeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AFIT3094_A1_CodeGameModeBase)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAFIT3094_A1_CodeGameModeBase(); \
	friend struct Z_Construct_UClass_AFIT3094_A1_CodeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AFIT3094_A1_CodeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AFIT3094_A1_CodeGameModeBase)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFIT3094_A1_CodeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFIT3094_A1_CodeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT3094_A1_CodeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT3094_A1_CodeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT3094_A1_CodeGameModeBase(AFIT3094_A1_CodeGameModeBase&&); \
	NO_API AFIT3094_A1_CodeGameModeBase(const AFIT3094_A1_CodeGameModeBase&); \
public:


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFIT3094_A1_CodeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT3094_A1_CodeGameModeBase(AFIT3094_A1_CodeGameModeBase&&); \
	NO_API AFIT3094_A1_CodeGameModeBase(const AFIT3094_A1_CodeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT3094_A1_CodeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT3094_A1_CodeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFIT3094_A1_CodeGameModeBase)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_PRIVATE_PROPERTY_OFFSET
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_15_PROLOG
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_RPC_WRAPPERS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_INCLASS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_INCLASS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT3094_A1_CODE_API UClass* StaticClass<class AFIT3094_A1_CodeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_FIT3094_A1_CodeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
