// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT3094_A1_CODE_ResourceActor_generated_h
#error "ResourceActor.generated.h already included, missing '#pragma once' in ResourceActor.h"
#endif
#define FIT3094_A1_CODE_ResourceActor_generated_h

#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_SPARSE_DATA
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_RPC_WRAPPERS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAResourceActor(); \
	friend struct Z_Construct_UClass_AResourceActor_Statics; \
public: \
	DECLARE_CLASS(AResourceActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AResourceActor)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAResourceActor(); \
	friend struct Z_Construct_UClass_AResourceActor_Statics; \
public: \
	DECLARE_CLASS(AResourceActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AResourceActor)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AResourceActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AResourceActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AResourceActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AResourceActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AResourceActor(AResourceActor&&); \
	NO_API AResourceActor(const AResourceActor&); \
public:


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AResourceActor(AResourceActor&&); \
	NO_API AResourceActor(const AResourceActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AResourceActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AResourceActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AResourceActor)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_PRIVATE_PROPERTY_OFFSET
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_10_PROLOG
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_RPC_WRAPPERS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_INCLASS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_INCLASS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT3094_A1_CODE_API UClass* StaticClass<class AResourceActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_ResourceActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
