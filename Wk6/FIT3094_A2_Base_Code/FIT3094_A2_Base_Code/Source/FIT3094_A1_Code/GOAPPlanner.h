// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Containers/Queue.h"
#include "CoreMinimal.h"

/**
 * 
 */
class AShip;
class GOAPAction;

struct GOAPNode
{
	// Where did this node come from?
	GOAPNode* Parent;
	// The cost so far to reach this point
	float RunningCost;
	// The current state of the world when reaching this point
	TMap<FString, bool> State;
	// A node is associated with an action. This stores a reference to that action
	GOAPAction* Action;
};

class GOAPPlanner
{
public:
	GOAPPlanner();
	~GOAPPlanner();

	// The function called to generate the action plan
	static bool Plan(
		AShip* Ship,
		const TSet<GOAPAction*>& AvailableActions,
		TQueue<GOAPAction*>& PlannedActions,
		TMap<FString, bool> WorldState,
		TMap<FString, bool> GoalState);

protected:
	// This function builds out the node graph until it exhausts all possible paths
	static bool BuildGraphRecursive(
		TArray<GOAPNode*>& AllNodes,
		GOAPNode* Parent,
		TArray<GOAPNode*>& GoalNodes,
		const TSet<GOAPAction*>& AvailableActions,
		TMap<FString, bool>& GoalState);

	// Function used for creating a subset of actions with on action removed
	static TSet<GOAPAction*> CreateActionSubset(
		const TSet<GOAPAction*>& AvailableActions,
		GOAPAction* RemoveAction);

	// Function compares a set of conditions to a state.
	// Used to both determine if goals are met and also if preconditions match existing world state
	static bool CheckConditionsInstate(
		TMap<FString, bool>& Conditions,
		TMap<FString, bool>& State);

	// Combine a current state with the change (aka effects) of an action
	static TMap<FString, bool> PopulateNewState(
		const TMap<FString, bool>& CurrentState,
		TMap<FString, bool>& Changes);
};
