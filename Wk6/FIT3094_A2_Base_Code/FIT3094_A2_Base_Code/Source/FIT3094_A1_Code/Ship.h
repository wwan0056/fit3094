// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GOAPAction.h"
#include "StateMachine.h"
#include "GridNode.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ship.generated.h"

class ALevelGenerator;


UCLASS()
class FIT3094_A1_CODE_API AShip : public AActor
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = "Stats")
	float MoveSpeed;
	UPROPERTY(EditAnywhere, Category = "Stats")
	float Tolerance;

public:
	// Valid states that the state machine can be in
	enum ACTOR_STATES
	{
		State_Nothing,
		State_Idle,
		State_Move,
		State_Action
	};

	// Sets default values for this actor's properties
	AShip();

	bool GeneratePath = true;
	bool FinishedMoving = false;
	TArray<GridNode*> Path;
	ALevelGenerator* Level;
	int morale = 100;

	int xPos;
	int yPos;

	GridNode::GRID_TYPE ResourceType;

	UPROPERTY()
	TArray<AActor*> PathDisplayActors;
	
	float MaxIdleTime;
	float CurrentIdleTime;

	int NumStone;
	int NumWood;
	int NumFruit;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// State Machine State Idle Functions
	void OnIdleEnter();
	void OnIdleTick(float DeltaTime);
	void OnIdleExit();

	// State Machine State Move Functions
	void OnMoveEnter();
	void OnMoveTick(float DeltaTime);
	void OnMoveExit();

	// State Machine State Action Functions
	void OnActionEnter();
	void OnActionTick(float DeltaTime);
	void OnActionExit();

	StateMachine<ACTOR_STATES, AShip>* ActionStateMachine;
	
	// A list of available actions this Agent can do
	TSet<GOAPAction*> AvailableActions;
	// The set of current actions the agent is planning to use
	TQueue<GOAPAction*> CurrentActions;

	// Function for getting the current world state
	TMap<FString, bool> GetWorldState();
	// Function for creating/determining a goal state we want to achieve.
	TMap<FString, bool> GetGoalState();

	// A plan was unable to be found. Handle this behaviour
	void OnPlanFailed(TMap<FString, bool> FailedGoalState);
	// A plan was aborted midway through
	void OnPlanAborted(GOAPAction* FailedAction);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
