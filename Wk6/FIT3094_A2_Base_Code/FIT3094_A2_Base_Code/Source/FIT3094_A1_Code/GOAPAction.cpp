// Fill out your copyright notice in the Description page of Project Settings.


#include "GOAPAction.h"

GOAPAction::GOAPAction()
{
	// nothing
}

GOAPAction::~GOAPAction()
{
	// nothing
}

void GOAPAction::DoReset()
{
	// Reset required variables
	InRage=false;
	Target=nullptr;

	// Call the child version of the reset function
	// Due to polymorphism
	Reset();
}

void GOAPAction::AddPrecondition(FString Name, bool State)
{
	Preconditions.Add(Name,State);
}

void GOAPAction::RemovePrecondition(FString Name, bool State)
{
	Preconditions.Remove(Name);
}

void GOAPAction::AddEffect(FString Name, bool State)
{
	Effects.Add(Name,State);
}

void GOAPAction::RemoveEffect(FString Name, bool State)
{
	Effects.Remove(Name);
}
