// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class AShip;

class GOAPAction
{
protected:
	bool InRage;

	virtual void Reset() =0;

public:
	// List of conditions required for this action to be valid
	TMap<FString, bool> Preconditions;
	// The changes this action has on the world
	TMap<FString, bool> Effects;

	// The cost of the action
	float ActionCost;

	AActor* Target;

	GOAPAction();
	~GOAPAction();

	// Reset the action to its base state
	void DoReset();

	// Adding and removing preconditions
	void AddPrecondition(FString Name, bool State);
	void RemovePrecondition(FString Name, bool State);

	// Adding and removing effects
	void AddEffect(FString Name, bool State);
	void RemoveEffect(FString Name, bool State);

	// Set and Check whether or not an Action is within range
	bool IsInRage() { return InRage; }
	void SetInRange(bool Range) { InRage = Range; }

	// Pure virtual functions that child classes MUST implement
	// Whether or not an action has finished executing
	virtual bool IsActionDone() = 0;
	// Check procedural preconditions at run time. Provided a Ship agent
	virtual bool CheckProceduralPreconditions(AShip* Ship) = 0;
	// Performs the Action
	// Returns true unless the action no longer able to be completed
	// This is called each frame until the action is done
	virtual bool PerformAction(AShip* Ship, float DeltaTime) = 0;
	// Whether or not the action requires something in range
	// Some Actions can be performed anywhere, whereas others require specific locations
	virtual bool RequiresInRange() =0;
};
