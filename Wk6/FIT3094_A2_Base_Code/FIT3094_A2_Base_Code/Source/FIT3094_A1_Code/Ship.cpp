// Fill out your copyright notice in the Description page of Project Settings.


#include "Ship.h"

#include "CollectTreasureAction.h"
#include "GOAPPlanner.h"
#include "Gold.h"
#include "LevelGenerator.h"

// Sets default values
AShip::AShip()
{
	// Set this actor to call Tick() every frame.
	PrimaryActorTick.bCanEverTick = true;
	MoveSpeed = 100;
	Tolerance = 1;

	ActionStateMachine = new StateMachine<ACTOR_STATES, AShip>(this, State_Nothing);
	ActionStateMachine->RegisterState(State_Idle, &AShip::OnIdleEnter, &AShip::OnIdleTick, &AShip::OnIdleExit);
	ActionStateMachine->RegisterState(State_Move, &AShip::OnMoveEnter, &AShip::OnMoveTick, &AShip::OnMoveExit);
	ActionStateMachine->RegisterState(State_Action, &AShip::OnActionEnter, &AShip::OnActionTick, &AShip::OnActionExit);
	ActionStateMachine->ChangeState(State_Idle);

	MaxIdleTime = 3;
	CurrentIdleTime = 0;

	CollectTreasureAction* TreasureAction = new CollectTreasureAction();
	TreasureAction->AddPrecondition("HasMorale", false);
	TreasureAction->AddEffect("HasMorale", true);
	AvailableActions.Add(TreasureAction);
}

// Called when the game starts or when spawned
void AShip::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ActionStateMachine->Tick(DeltaTime);
}

void AShip::OnIdleEnter()
{
}

void AShip::OnIdleTick(float DeltaTime)
{
	FinishedMoving = true;
	if (CurrentIdleTime >= MaxIdleTime)
	{
		CurrentIdleTime = 0;

		TMap<FString, bool> WorldState = GetWorldState();
		TMap<FString, bool> GoalState = GetGoalState();

		// Attempt to make a plan and check success
		if (GOAPPlanner::Plan(this, AvailableActions, CurrentActions, WorldState, GoalState))
		{
			UE_LOG(LogTemp, Warning, TEXT("%s has found a plan. Executing plan"), *GetName());
			ActionStateMachine->ChangeState(State_Action);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("%s was unable to find a plan. Idling for %f secondes"), *GetName(),
			       MaxIdleTime);
		}
	}
	else
	{
		CurrentIdleTime += DeltaTime;
	}
}

void AShip::OnIdleExit()
{
}

void AShip::OnMoveEnter()
{
	// Entering into the move state check to ensure we have some form of movement to ocmplete
	// If we do not then cancel immediately
	if (CurrentActions.IsEmpty())
	{
		ActionStateMachine->ChangeState(State_Idle);
		return;
	}

	// If current action requires an InRange check AND the target is null return to planning
	GOAPAction* CurrentAction = *CurrentActions.Peek();
	if (CurrentAction->RequiresInRange() && CurrentAction->Target == nullptr)
	{
		ActionStateMachine->ChangeState(State_Idle);
		return;
	}

	// Else lets make a plan to move to the new action
	if (CurrentAction->RequiresInRange())
	{
		GridNode* GoalLocation = Level->FindGridNode(CurrentAction->Target);
		if (GoalLocation)
		{
			Level->CalculatePath(this, GoalLocation);
		}
	}
}

void AShip::OnMoveTick(float DeltaTime)
{
	// Get the current action we are executing
	// This will never be null as CurrentActions must have a count to enter this state
	GOAPAction* CurrentAction = *CurrentActions.Peek();

	if (!FinishedMoving)
	{
		if (Path.Num() > 0)
		{
			// Get our current location
			FVector CurrentPosition = GetActorLocation();

			// Calculate the target position based off the X & Y values inside of path
			float TargetXPos = Path[0]->X * ALevelGenerator::GRID_SIZE_WORLD;
			float TargetYPos = Path[0]->Y * ALevelGenerator::GRID_SIZE_WORLD;
			FVector TargetPosition(TargetXPos, TargetYPos, CurrentPosition.Z);

			FVector Direction = TargetPosition - CurrentPosition;
			Direction.Normalize();

			CurrentPosition += Direction * MoveSpeed * DeltaTime;
			SetActorLocation(CurrentPosition);

			if (FVector::Dist(CurrentPosition, TargetPosition) <= Tolerance)
			{
				xPos = Path[0]->X;
				yPos = Path[0]->Y;
				CurrentPosition = TargetPosition;
				Path.RemoveAt(0);
				morale--;

				FinishedMoving = true;
			}
		}
		else
		{
			// Set the current action to be in range. We have moved to its location so this is valid
			CurrentAction->SetInRange(true);
			// Change to the Action state. Time to perform our action
			ActionStateMachine->ChangeState(State_Action);
		}
	}
}

void AShip::OnMoveExit()
{
}

void AShip::OnActionEnter()
{
}

void AShip::OnActionTick(float DeltaTime)
{
	// We do not need to move so set this to true
	FinishedMoving = true;
	// If we have no state change to idle and exit immediately
	if (CurrentActions.IsEmpty())
	{
		ActionStateMachine->ChangeState(State_Idle);
		return;
	}

	// Check to see if our action has finished
	GOAPAction* CurrentAction = *CurrentActions.Peek();
	if (CurrentAction->IsActionDone())
	{
		// we have finished with the action. Lets get rid of it
		CurrentActions.Dequeue(CurrentAction);

		// Delete all path actors once the goal has been reached
		// These are here to help visualize our path. We are done with them
		for (auto PathObject : PathDisplayActors)
		{
			PathObject->Destroy();
		}
		PathDisplayActors.Empty();
	}

	// If at this point we still have more actions continue the process
	if (!CurrentActions.IsEmpty())
	{
		// Get the top of the queue again
		CurrentAction = *CurrentActions.Peek();

		// CHeck to see if we need to be within range for an action
		bool InRange = CurrentAction->RequiresInRange() ? CurrentAction->IsInRage() : true;

		// If we are in range attempt the action
		if (InRange)
		{
			// Attempt to perform the action
			bool IsActionSuccessful = CurrentAction->PerformAction(this, DeltaTime);

			// If we fail the action change to the Idle state and report that we had to abort the plan
			if (!IsActionSuccessful)
			{
				ActionStateMachine->ChangeState(State_Idle);
				OnPlanAborted(CurrentAction);
			}
		}
		else
		{
			// At this point we have a valid action but we are not in range. Commence movement
			ActionStateMachine->ChangeState(State_Move);
		}
	}
	else
	{
		// No Actions remaining. Return to Idle State
		ActionStateMachine->ChangeState(State_Idle);
	}
}

void AShip::OnActionExit()
{
}

TMap<FString, bool> AShip::GetWorldState()
{
	TMap<FString, bool> WorldState;

	WorldState.Add("HasMorale", morale > 100);

	return WorldState;
}

TMap<FString, bool> AShip::GetGoalState()
{
	TMap<FString, bool> GoalState;

	GoalState.Add("HasMorale", true);

	return GoalState;
}

void AShip::OnPlanFailed(TMap<FString, bool> FailedGoalState)
{
	UE_LOG(LogTemp, Warning, TEXT("GOAP ACTOR CLASS: Plan Failed"));
}

void AShip::OnPlanAborted(GOAPAction* FailedAction)
{
	UE_LOG(LogTemp, Warning, TEXT("GOAP ACTOR CLASS: Plan Aborted"));
}
